import { promises as dns } from 'dns';
import { join } from 'path';
import { tmpdir } from 'os';
import { execSync } from 'child_process';
import { existsSync, mkdirSync, writeFileSync, readFileSync } from 'fs';

export default class CheckDns {
  constructor(settings) {
    this.settings = settings;
    this.cache = {};
    this.actions = [];
    this.dir = join(tmpdir(), 'check-dns');
    this.cacheFile = join(this.dir, 'cache.json');
    this.init();
  }

  init() {
    this.initCache();
    this.checkHosts();
  }

  initCache() {
    if (!existsSync(this.dir)) {
      mkdirSync(this.dir);
    }
    if (!existsSync(this.cacheFile)) {
      this.writeCache();
    }

    this.readCache();
  }

  readCache() {
    const data = readFileSync(this.cacheFile, 'utf8');
    this.cache = JSON.parse(data);
  }

  writeCache() {
    writeFileSync(this.cacheFile, JSON.stringify(this.cache));
  }

  async checkHosts() {
    const results = await Promise.allSettled(
      this.settings.map(item => dns.lookup(item.host))
    );
    results.forEach((result, index) => {
      if(result.status === 'fulfilled'){
        const host = this.settings[index].host;
        if (this.cache[host] !== result.value.address){
          this.actions.push(this.settings[index].action)
          this.cache[host] = result.value.address;
        }
      }
    })

    this.execActions();
    this.writeCache();
  }

  execActions(){
    this.actions.forEach(action => {
      console.log('Executing > %j', action);
      console.log(execSync(action).toString());
    })
  }
}
